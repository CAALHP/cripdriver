﻿using System.AddIn;
using Plugins.DriverPluginAdapters;

namespace Plugins.CRIPDriver
{
    // The AddInAttribute identifies this pipeline segment as an add-in.
    [AddIn("CRIP Driver Plugin", Version = "1.0.0.0")]
    public class CRIPDeviceDriver : DeviceDriverViewPluginAdapter
    {
        public CRIPDeviceDriver()
        {
            DeviceDriver = new CRIPDeviceDriverImplementation();
            
        }
    }
}