﻿using System;
using System.Collections.Generic;
using System.Threading;
using CAALHP.Contracts;
using CAALHP.Events;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using CAALHP.Utils.Helpers.Serialization.JSON;
using Phidgets;
using Phidgets.Events;

namespace Plugins.CRIPDriver
{
    public class CRIPDeviceDriverImplementation : IDeviceDriverCAALHPContract
    {
        private IDeviceDriverHostCAALHPContract _host;
        private int _processId;
        private RFID _rfid;

        public CRIPDeviceDriverImplementation()
        {
            //InitRFID();
        }

        ~CRIPDeviceDriverImplementation()
        {
            DisposeRFID();
        }

        public double GetMeasurement()
        {
            var rnd = new System.Random();
            return rnd.NextDouble();
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            //throw new System.NotImplementedException();
        }

        public string GetName()
        {
            return "CRIPDriver";
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Environment.Exit(0);
        }

        public void Initialize(IDeviceDriverHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
            InitRFID();
        }

        private void InitRFID()
        {
            _rfid = new RFID();
            _rfid.Attach += rfid_Attach;
            _rfid.Detach += rfid_Detach;
            _rfid.Error += rfid_Error;

            _rfid.Tag += rfid_Tag;
            _rfid.TagLost += rfid_TagLost;
            _rfid.open();

            //Wait for a Phidget RFID to be attached before doing anything with 
            //the object
            _rfid.waitForAttachment();

            //turn on the antenna and the led to show everything is working
            _rfid.Antenna = true;
            _rfid.LED = true;
        }

        private void DisposeRFID()
        {
            //turn off the led
            if (_rfid != null)
            {
                try
                {
                    _rfid.LED = false;
                    //close the phidget and dispose of the object
                    _rfid.close();
                }
                finally
                {
                    _rfid = null;    
                }
            }
        }

        //attach event handler...display the serial number of the attached RFID phidget
        void rfid_Attach(object sender, AttachEventArgs e)
        {
            if (_host == null) return;
            var info = new GenericInfoEvent()
            {
                CallerName = GetName(),
                CallerProcessId = _processId,
                Info = "RFID_ATTACHED"
            };
            var theEvent = EventHelper.CreateEvent(SerializationType.Json, info);
            _host.Host.ReportEvent(theEvent);
        }

        //detach event handler...display the serial number of the detached RFID phidget
        void rfid_Detach(object sender, DetachEventArgs e)
        {
            if (_host == null) return;
            var info = new GenericInfoEvent()
            {
                CallerName = GetName(),
                CallerProcessId = _processId,
                Info = "RFID_DETACHED"
            };
            var theEvent = EventHelper.CreateEvent(SerializationType.Json, info);
            _host.Host.ReportEvent(theEvent);
        }

        //Error event handler...display the error description string
        void rfid_Error(object sender, ErrorEventArgs e)
        {
            if (_host == null) return;
            var error = new ErrorEvent()
            {
                CallerName = GetName(),
                CallerProcessId = _processId,
                ErrorMessage = e.Description
            };
            var theEvent = EventHelper.CreateEvent(SerializationType.Json, error);
            _host.Host.ReportEvent(theEvent);
        }

        //Print the tag code of the scanned tag
        void rfid_Tag(object sender, TagEventArgs e)
        {
            if (_host == null) return;
            var rfidFoundEvent = new RFIDFoundEvent
            {
                Tag = e.Tag,
                CallerName = GetName(),
                CallerProcessId = _processId,
            };
            var theEvent = EventHelper.CreateEvent(SerializationType.Json, rfidFoundEvent);
            //var json = JsonSerializer.SerializeEvent(rfidFoundEvent);
            //var theEvent = EventHelper.CreateEvent(key, json);
            //_host.Host.ReportEvent(EventHelper.CreateEvent(_processId, GetName(), "RFID_FOUND", e.Tag));
            _host.Host.ReportEvent(theEvent);
            Thread.Sleep(1000);
        }

        //print the tag code for the tag that was just lost
        void rfid_TagLost(object sender, TagEventArgs e)
        {
            if (_host == null) return;
            var rfidFoundEvent = new RFIDLostEvent
            {
                Tag = e.Tag,
                CallerName = GetName(),
                CallerProcessId = _processId,
            };
            var theEvent = EventHelper.CreateEvent(SerializationType.Json, rfidFoundEvent);
            _host.Host.ReportEvent(theEvent);
            Thread.Sleep(1000);
            //_host.Host.ReportEvent(EventHelper.CreateEvent(_processId, GetName(), "RFID_LOST", e.Tag));
        }
    }
}