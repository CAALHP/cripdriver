﻿param($installPath, $toolsPath, $package, $project)

$file1 = $project.ProjectItems.Item("Pipeline").ProjectItems.Item("AddInViews").ProjectItems.Item("CAALHP.Contracts.dll")
$file2 = $project.ProjectItems.Item("Pipeline").ProjectItems.Item("AddInViews").ProjectItems.Item("Plugins.AddInViews.dll")

# set 'Copy To Output Directory' to 'Copy if newer'
$copyToOutput1 = $file1.Properties.Item("CopyToOutputDirectory")
$copyToOutput1.Value = 2

$copyToOutput2 = $file2.Properties.Item("CopyToOutputDirectory")
$copyToOutput2.Value = 2

$project.Object.References | Where-Object { $_.Name -eq 'AddInViews-RefOnly' } | ForEach-Object { Write-Host "removing reference: " $_.Name }
$project.Object.References | Where-Object { $_.Name -eq 'AddInViews-RefOnly' } | ForEach-Object { $_.Remove() }

$project.Object.References | Where-Object { $_.Name -eq 'Plugins.AddInViews' } | ForEach-Object { Write-Host "setting copy local = false on : " $_.Name }
$project.Object.References | Where-Object { $_.Name -eq 'Plugins.AddInViews' } | ForEach-Object { $_.CopyLocal = $false; }
